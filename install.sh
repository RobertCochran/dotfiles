#!/bin/bash

for i in $(find -mindepth 1 -name ".*" -not -name ".git"); do
    ln -s $(realpath "$i") $(realpath ~/"$i")
done

# Handle systemd directory special because it is different
echo ln -s $(realpath systemd) ~/.config/systemd
