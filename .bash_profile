# .bash_profile

# User specific environment and startup programs

# Make system bell more quiet
#amixer -q -c 1 set Beep 10

#export PATH=$HOME/bin:$PATH:$HOME/.local/bin
export EDITOR="emacsclient -a '' -c"

