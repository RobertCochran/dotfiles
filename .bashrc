# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi

# Not running in a raw TTY or Emacs, tmux available, and not currently running?
if [[ ! $(tty) =~ "/dev/tty" ]] && [ -z "$INSIDE_EMACS" ] && command -v tmux >/dev/null 2>&1 && [ -z "$TMUX" ]; then
    # Are there any loose sessions? Reuse them.
    if tmux ls | grep "main" | grep -vq "(attached)"; then
	loose_session=$(tmux ls | grep "main" | grep -v "(attached)" | cut -d: -f1 | head -n1)
	exec tmux attach -t "$loose_session" \; new-window
    else
	exec tmux new-session -t main
    fi
fi

# Put your fun stuff here.

if [ "$TERM" == "dumb" ]; then
	export PS1="[\u@\h \W] $ "
else
	export PS1="\[\033[01;34m\][\[\033[01;36m\]\u\[\033[01;34m\]@\[\033[01;36m\]\h\[\033[01;34m\] \W] \[\033[01;36m\]$\[\033[00m\] "
fi

alias ls="ls --color=auto -F"
alias emacs-t="emacsclient -a '' -t"
alias emacs-x="emacsclient -a '' -c"

clear
fortune -s | cowsay -f tux
echo "Time is `date`"
